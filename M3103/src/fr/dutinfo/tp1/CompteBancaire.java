package fr.dutinfo.tp1;

import fr.dutinfo.utils.Couple;
import fr.dutinfo.utils.OutilsFinanciers;
import fr.dutinfo.utils.ParametreInvalide;

public abstract class CompteBancaire {

	private String libelle;
	private float solde;
	private Devise devise;
	
	public CompteBancaire(String libelle, Devise devise) {
		this.libelle = libelle;
		this.devise = devise;
	}

	public Devise getDevise() {
		return devise;
	}
	
	public void crediter(Couple<Devise, Float> ajout) throws ParametreInvalide {
		if(ajout.getValeur2() < 0) {
			throw new ParametreInvalide("Le montant ne peut �tre n�gatif", "ajout");
		}
		if(ajout.getValeur1() != this.devise) {
			ajout = OutilsFinanciers.convertir(ajout, this.devise);
		}
		this.solde+=ajout.getValeur2();
	}
	
	protected void setSolde(float solde) {
		this.solde = solde;
	}
	
	public float getSolde() {
		return solde;
	}
	
	public String getLibelle() {
		return libelle;
	}
	
	@Override
	public String toString() {
		return "Compte Bancaire : "+libelle.toUpperCase()+"("+solde+" - "+devise.toString()+")";
	}
	
}
