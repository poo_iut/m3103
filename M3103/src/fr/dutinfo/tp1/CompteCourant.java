package fr.dutinfo.tp1;

import fr.dutinfo.utils.Couple;
import fr.dutinfo.utils.OutilsFinanciers;
import fr.dutinfo.utils.ParametreInvalide;

public class CompteCourant extends CompteBancaire {

	public CompteCourant(String libelle, Devise devise) {
		super(libelle, devise);
		// TODO Auto-generated constructor stub
	}

	public void debiter(Couple<Devise, Float> montant) throws ParametreInvalide {
		if(montant.getValeur2() < 0) {
			throw new ParametreInvalide("Le montant ne peut �tre n�gatif", "ajout");
		}
		if(montant.getValeur1() != getDevise()) {
			montant = OutilsFinanciers.convertir(montant, getDevise());
		}
		float nouveauSolde = getSolde() - montant.getValeur2();
		setSolde(nouveauSolde);
	}
	
}
