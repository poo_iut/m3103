package fr.dutinfo.tp1;

public enum Devise {

	EURO("EUR"),
	
	DOLLAR("USD");
	
	String id;
	
	private Devise(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
}
