package fr.dutinfo.tp1;

import java.util.Arrays;

import fr.dutinfo.utils.Couple;
import fr.dutinfo.utils.ParametreInvalide;

public class Main {

	public static void main(String[] args) {
		
		try {
			
			Banque banque = new Banque(50);

			CompteCourant cc = new CompteCourant("AH14GDX", Devise.EURO);
			cc.crediter(new Couple<Devise, Float>(Devise.EURO, 50f));
			cc.crediter(new Couple<Devise, Float>(Devise.DOLLAR, 30f));
			System.out.println(cc);
			System.out.println(cc);
			
			System.out.println("");
			
			CompteCourant cc2 = new CompteCourant("AH15GDX", Devise.DOLLAR);
			cc2.crediter(new Couple<Devise, Float>(Devise.EURO, 50f));
			System.out.println(cc2);
			cc2.crediter(new Couple<Devise, Float>(Devise.DOLLAR, 30f));
			System.out.println(cc2);

			banque.ajouterCompte(cc);
			banque.ajouterCompte(cc2);
			System.out.println(Arrays.toString(banque.getTableauCompte()));
			banque.retirerCompte(cc);
			System.out.println(Arrays.toString(banque.getTableauCompte()));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
