package fr.dutinfo.tp1;

import fr.dutinfo.utils.Couple;
import fr.dutinfo.utils.DuplicationCompte;
import fr.dutinfo.utils.ParametreInvalide;

public class Banque {

	public static void virer(CompteBancaire cb1, float montant, CompteBancaire cb2) throws ParametreInvalide {
		cb2.crediter(new Couple<Devise, Float>(cb1.getDevise(), montant));
	}
	
	
	private int tailleMaximale;
	private int dernierCompte;
	private CompteBancaire[] tableauCompte;
	
	public Banque(int tailleMax) {
		this.tailleMaximale = tailleMax;
		this.tableauCompte = new CompteBancaire[this.tailleMaximale];
		this.dernierCompte = 0;
	}
	
	public boolean hasCompte(String libelle) {
		for(int i = 0; i < dernierCompte; i++) {
			if(this.tableauCompte[i] != null) {
				if(this.tableauCompte[i].getLibelle().contentEquals(libelle)) return true;
			}
		}
		return false;
	}
	
	public void ajouterCompte(CompteBancaire compte) {
		if(hasCompte(compte.getLibelle())) {
			throw new DuplicationCompte();
		}
		this.tableauCompte[this.dernierCompte] = compte;
		this.dernierCompte++;
	}
	
	public void retirerCompte(CompteBancaire compte) {
		if(!hasCompte(compte.getLibelle())) {
			throw new AbsenceCompte();
		}
		int indexDuCompte = 0;
		for(; indexDuCompte < this.tailleMaximale && this.tableauCompte[indexDuCompte] != compte; indexDuCompte++) {}
		CompteBancaire dernierCompte = this.tableauCompte[this.dernierCompte-1];
		if(dernierCompte == compte) {
			this.tableauCompte[this.dernierCompte] = null;
		}else {
			this.tableauCompte[indexDuCompte] = dernierCompte;
			this.tableauCompte[this.dernierCompte-1] = null;
		}
		this.dernierCompte--;
	}
	
	
	public CompteBancaire[] getTableauCompte() {
		return tableauCompte;
	}
	
	
	
	
}
