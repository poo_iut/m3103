package fr.dutinfo.tp3;

import fr.dutinfo.tp3.alter.Graphe;
import fr.dutinfo.tp3.alter.Noeud;
import fr.dutinfo.utils.Couple;
import fr.dutinfo.utils.StackTraceDumper;
import fr.dutinfo.utils.TableauUtil;

public class Labyrinthe {

	public static final int[][] labyrinthe = 
		{
				//0  1  2  3  4  5  6  7  8  9 <= X
				{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
				{ 1, 1, 0, 0, 0, 0, 1, 1, 0, 1 },
				{ 1, 1, 0, 1, 0, 1, 1, 0, 0, 1 },
				{ 1, 0, 0, 1, 0, 0, 1, 0, 1, 1 },
				{ 1, 0, 1, 0, 0, 1, 1, 0, 0, 1 },
				{ 1, 0, 1, 1, 0, 1, 1, 1, 0, 1 },
				{ 1, 0, 1, 1, 0, 0, 0, 0, 0, 0 },
				{ 1, 0, 0, 0, 0, 1, 1, 1, 1, 1 },
				{ 1, 0, 1, 1, 0, 0, 0, 0, 0, 1 },
				{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
		};
	
	private int[][] copie = TableauUtil.cloneArray(labyrinthe);
	
	private Couple<Integer, Integer> fin;
	
	public Labyrinthe() {
		this.fin = new Couple<>(9, 6);
	}
	
	public void reset() {
		this.copie = TableauUtil.cloneArray(labyrinthe);
	}
	
	public boolean isMur(int x, int y) {
		return this.copie[y][x] == 1;
	}
	
	public boolean isBien(int x, int y) {
		return this.copie[y][x] == 0;
	}
	
	public boolean isVisité(int x, int y) {
		return this.copie[y][x] == 2;
	}
	
	public void setVisité(int x, int y) {
		this.copie[y][x] = 2;
	}
	
	public void setNotVisité(int x, int y) {
		this.copie[y][x] = 0;
	}
	
	
	public void trouverGraphe(int x, int y) {
		Graphe graphe = new Graphe();
		existeCheminGraphe(graphe, x, y);
		for(int i = 0; i < labyrinthe.length; i++) {
			String ligne = "";
			for(int j = 0; j < labyrinthe[i].length; j++) {
				Noeud noeud = graphe.trouverNoeud(j, i);
				if(noeud != null) {
					ligne+="["+noeud.getCaseX()+"-"+noeud.getCaseY()+"]   ";
				}else {
					ligne+="        ";
				}
			}
			System.out.println(ligne);
		}
	}
	
	private boolean existeCheminGraphe(Graphe graphe, int x, int y) {
		if(this.fin.getValeur1().equals(x) && this.fin.getValeur2().equals(y)) {
			setVisité(x, y);
			return true;
		}
		if(x < 0 || x > 9)return false;
		if(y < 0 || y > 9) return false;
		if(isMur(x, y))return false;
		Noeud actuel = new Noeud(x, y) ;
		if(isBien(x+1, y)) {
			setVisité(x, y);
			if(existeCheminGraphe(graphe, x+1, y)) {
				graphe.ajouterLien(actuel, x+1, y);
				setNotVisité(x, y);
				return true;
			}
		}
		if(isBien(x, y-1)) {
			setVisité(x, y);
			if(existeCheminGraphe(graphe, x, y-1)){
				graphe.ajouterLien(actuel, x, y-1);
				setNotVisité(x, y);
				return true;
			}
		}
		if(isBien(x, y+1)) {
			setVisité(x, y);
			if(existeCheminGraphe(graphe, x, y+1)){
				graphe.ajouterLien(actuel, x, y+1);
				setNotVisité(x, y);
				return true;
			}
		}
		if(isBien(x-1, y)) {
			setVisité(x, y);
			if(existeCheminGraphe(graphe, x-1, y)) {
				graphe.ajouterLien(actuel, x-1, y);
				setNotVisité(x, y);
				return true;
			}
		}
		return false;
	}
	
	private boolean existeChemin(StringBuilder builder, int x, int y) {
		if(this.fin.getValeur1().equals(x) && this.fin.getValeur2().equals(y)) {
			setVisité(x, y);
			return true;
		}
		if(x < 0 || x > 9)return false;
		if(y < 0 || y > 9) return false;
		if(isMur(x, y))return false;
		int current = builder.length();
		if(isBien(x+1, y)) {
			setVisité(x, y);
			builder.append("Droite, ");
			if(existeChemin(builder, x+1, y)) {
				return true;
			}
		}
		builder.replace(current, builder.length(), "");
		if(isBien(x, y-1)) {
			setVisité(x, y);
			builder.append("Haut, ");
			if(existeChemin(builder, x, y-1)){
				return true;
			}
		}
		builder.replace(current, builder.length(), "");
		if(isBien(x, y+1)) {
			setVisité(x, y);
			builder.append("Bas, ");
			if(existeChemin(builder, x, y+1)){
				return true;
			}
		}
		builder.replace(current, builder.length(), "");
		if(isBien(x-1, y)) {
			setVisité(x, y);
			builder.append("Gauche, ");
			if(existeChemin(builder, x-1, y)) {
				return true;
			}
		}
		setVisité(x, y);
		return false;
	}
	
	public String existeChemin(int x, int y) {
		StringBuilder builder = new StringBuilder();
		existeChemin(builder, x, y);
		return builder.toString();
	}
	
	public void afficherLabyrinthe() {
		System.out.println("==========");
		for(int i = 0; i < this.copie.length; i++) {
			String ligne = "";
			for(int j = 0; j < this.copie[i].length; j++) {
				switch (this.copie[i][j]) {
					case 0:
						ligne+="  ";
						break;
					case 1:
						ligne+="MM";
						break;
					case 2:
						ligne+="--";
						break;
				}
			}
			System.out.println(ligne);
		}
		System.out.println("==========");
	}
	
}
