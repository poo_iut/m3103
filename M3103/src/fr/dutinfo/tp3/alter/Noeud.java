package fr.dutinfo.tp3.alter;

import java.util.ArrayList;
import java.util.List;

public class Noeud {

	private int caseX;
	private int caseY;
	private List<Noeud> lien;

	public Noeud(int caseX, int caseY) {
		super();
		this.caseX = caseX;
		this.caseY = caseY;
		this.lien = new ArrayList<Noeud>();
	}

	public int getCaseX() {
		return caseX;
	}

	public int getCaseY() {
		return caseY;
	}

	public boolean hasLien() {
		return !this.lien.isEmpty();
	}

	public boolean hasMultipleLien() {
		return this.lien.size() > 1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + caseX;
		result = prime * result + caseY;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Noeud other = (Noeud) obj;
		if (caseX != other.caseX)
			return false;
		if (caseY != other.caseY)
			return false;
		return true;
	}

	public void addNoeud(Noeud noeud) {
		if(!this.lien.contains(noeud)) {
			this.lien.add(noeud);
		}
	}
	
	public List<Noeud> getLien() {
		return lien;
	}

	@Override
	public String toString() {
		return "Noeud [caseX=" + caseX + ", caseY=" + caseY + "]";
	}
	
	

}
