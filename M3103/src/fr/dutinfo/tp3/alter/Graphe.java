package fr.dutinfo.tp3.alter;

import java.util.ArrayList;
import java.util.List;

public class Graphe {

	private List<Noeud> noeud;

	public Graphe() {
		this.noeud = new ArrayList<Noeud>();
	}

	public void addNoeud(Noeud noeud) {
		this.noeud.add(noeud);
	}

	public Noeud trouverNoeud(int x, int y) {
		// System.out.println(this.noeud);
		return this.noeud.stream().filter(n -> n.getCaseX() == x && n.getCaseY() == y).findFirst().orElse(null);
	}

	public void ajouterLien(Noeud precedent, int x, int y) {
		if(!this.noeud.contains(precedent)) {
			this.noeud.add(precedent);
		}
		Noeud prec = this.noeud.get(this.noeud.indexOf(precedent));
		Noeud nouveau = new Noeud(x, y);
		if (trouverNoeud(x, y) != null) {
			nouveau = trouverNoeud(x, y);
		} else {
			this.noeud.add(nouveau);
		}
		prec.addNoeud(nouveau);
		nouveau.addNoeud(prec);
	}

}
