package fr.dutinfo.utils;

import java.util.Map;

public class StackTraceDumper {
	
	public static final String STACK_HEADER = 
			"|  %thread%  |";
	
	public static final String STACK_CONTENT = 
			"|  -%n%-  | %content%";
	
	public static void dumpAllStackTraces() {
		for (Map.Entry<Thread, StackTraceElement[]> entry : Thread.getAllStackTraces().entrySet()) {
			if (entry.getKey().getName().contains("main")) {
				System.out.println(entry.getKey().getName() + ":");
				for (StackTraceElement element : entry.getValue()) {
					System.out.println("\t" + element);
				}
			}
		}
	}
	
	public static void printCleanStack() {
		for(Map.Entry<Thread, StackTraceElement[]> entry : Thread.getAllStackTraces().entrySet()) {
			if (entry.getKey().getName().contains("main")) {
				System.out.println(STACK_HEADER.replace("%thread%", entry.getKey().getName()));
				int n = 0;
				for (StackTraceElement element : entry.getValue()) {
					n++;
					System.out.println(STACK_CONTENT
										.replace("%content%", element.toString())
										.replace("%n%", String.valueOf(n)));
				}
			}
		}
	}
}