package fr.dutinfo.utils;

public class Couple<X, Y> {

	private X valeur1;
	private Y valeur2;
	
	public Couple(X valeur1, Y valeur2) {
		this.valeur1 = valeur1;
		this.valeur2 = valeur2;
	}

	public X getValeur1() {
		return valeur1;
	}
	
	public Y getValeur2() {
		return valeur2;
	}
	
}
