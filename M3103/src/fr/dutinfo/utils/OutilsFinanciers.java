package fr.dutinfo.utils;

import fr.dutinfo.tp1.Devise;

public class OutilsFinanciers {

	public static float tauxDeChange(Devise devise, Devise devise2) {
		if(devise == Devise.EURO) {
			if(devise2 == Devise.EURO) {
				return 1f;
			}
			return 1.31486f;
		}
		if(devise2 == Devise.DOLLAR) {
			return 1f;
		}
		return 0.760541f;
	}
	
	public static Couple<Devise, Float> convertir(Couple<Devise, Float> couple, Devise nouvelleDevise){
		return new Couple<Devise, Float>(nouvelleDevise, tauxDeChange(couple.getValeur1(), nouvelleDevise)*couple.getValeur2());
	}
	
}
