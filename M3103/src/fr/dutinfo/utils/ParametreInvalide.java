package fr.dutinfo.utils;

public class ParametreInvalide extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1907547068501481770L;

	public ParametreInvalide(String parametreInvalide, String raisonErreur) {
		super("Raison de l'erreur: "+raisonErreur+", Param�tre invalide : "+parametreInvalide);
	}
	
}
