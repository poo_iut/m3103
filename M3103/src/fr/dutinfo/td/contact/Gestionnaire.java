package fr.dutinfo.td.contact;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Gestionnaire {

	private Map<String, Set<Contact>> dictionnaire;
	
	public Gestionnaire() {
		dictionnaire = new HashMap<>();
	}

	public Contact rechercherContact(String nom, String prenom) {
		if(nom == null || prenom == null) {
			return null;
		}
		String clé = prenom+" "+nom;
		if(!this.dictionnaire.containsKey(clé)) {
			return null;
		}
		return new ArrayList<>(this.dictionnaire.get(clé)).get(0);
	}
	
	public void ajouterContact(Contact contact) {
		if(contact == null) return;
		String clé = contactVersChaine(contact);
		if(dictionnaire.containsKey(clé)) {
			dictionnaire.get(clé).add(contact);
		}else {
			Set<Contact> contacts = new TreeSet<Contact>();
			contacts.add(contact);
			dictionnaire.put(clé, contacts);
		}
	}
	
	private String contactVersChaine(Contact contact) {
		return contact.getNom()+" "+contact.getPrenom();
	}
	
	@Override
	public String toString() {
		List<String> keys = new ArrayList<String>(dictionnaire.keySet());
		Collections.sort(keys);
		StringBuilder builder = new StringBuilder();
		for(String clé : keys) {
			for(Contact contact : dictionnaire.get(clé)) {
				builder.append(" -"+contact.toString()+"\n");
			}
		}
		return builder.toString();
	}
	
}
