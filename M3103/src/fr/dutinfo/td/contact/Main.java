package fr.dutinfo.td.contact;

public class Main {

	public static void main(String[] args) {
		Gestionnaire gestionnaire = new Gestionnaire();
		Contact c1 = new Contact("Fabien", "CAYRE", "0102030405");
		Contact c2 = new Contact("Dorian", "GARDES", "0102030405");
		Contact c3 = new Contact("Guilhem", "FOUCHOU", "0102030405");
		Contact c4 = new Contact("Dorian", "GARDES", "1122334455");
		Contact c5 = new Contact("Fabrice", "GARDET", "2233445566");
		gestionnaire.ajouterContact(c1);
		gestionnaire.ajouterContact(c2);
		gestionnaire.ajouterContact(c3);
		gestionnaire.ajouterContact(c4);
		gestionnaire.ajouterContact(c5);
		System.out.println(gestionnaire.toString());
	}

}
