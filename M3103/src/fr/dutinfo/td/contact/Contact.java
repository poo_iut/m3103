package fr.dutinfo.td.contact;

import java.util.Objects;

public class Contact implements Comparable<Contact>{

	private String nom;
	private String prenom;
	private String telephone;
	
	public Contact(String prenom, String nom, String telephone) {
		Objects.requireNonNull(prenom);
		Objects.requireNonNull(nom);
		Objects.requireNonNull(telephone);
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getTelephone() {
		return telephone;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result+=prime*this.prenom.hashCode();
		result+=prime*this.nom.hashCode();
		result+=prime*this.telephone.hashCode();
		return result;
	}
	
	@Override
	public String toString() {
		return this.prenom+" "+this.nom+" "+this.telephone;
	}
	
	@Override
	public int compareTo(Contact o) {
		return toString().compareTo(o.toString());
	}
	
	
	
}
