package fr.dutinfo.td.arbre;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		System.out.println("Veuillez saisir la liste des mots");
		System.out.println("(Saisissez \"fin\" quand fini)");
		Scanner scanner = new Scanner(System.in);
		ArbreBinaire<String> arbre = new ArbreBinaire<>();
		String line = "";
		while(!(line = scanner.nextLine()).equals("fin")) {
			arbre.insererElement(line);
		}
		scanner.close();
		System.out.println(arbre.toString());
		nombre();
	}
	
	public static void nombre() {
		ArbreBinaire<Integer> arbre = new ArbreBinaire<Integer>();
		arbre.insererElement(1);
		arbre.insererElement(-3);
		arbre.insererElement(-2);
		arbre.insererElement(-1);
		arbre.insererElement(2);
		arbre.insererElement(3);
		arbre.insererElement(5);
		arbre.insererElement(4);
		arbre.insererElement(19);
		arbre.insererElement(7);
		arbre.insererElement(8);
		System.out.println(arbre.toString());
	}

}
