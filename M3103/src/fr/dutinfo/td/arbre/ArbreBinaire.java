package fr.dutinfo.td.arbre;

public class ArbreBinaire<T extends Comparable<T>> {

	private T racine;
	private ArbreBinaire<T> filsDroit;
	private ArbreBinaire<T> filsGauche;
	
	/**
	 * Arbre vide
	 */
	public ArbreBinaire() {
		this(null, null, null);
	}

	public ArbreBinaire(T racine, ArbreBinaire<T> filsDroit, ArbreBinaire<T> filsGauche) {
		this.racine = racine;
		this.filsDroit = filsDroit;
		this.filsGauche = filsGauche;
	}
	
	public void insererElement(T element) {
		if(estVide()) {
			this.racine = element;
		}else {
			if(!this.racine.equals(element)) {
				if(element.compareTo(this.racine) < 0) {
					if(this.filsGauche != null) {
						this.filsGauche.insererElement(element);
					}else {
						this.filsGauche = new ArbreBinaire<>(element, null, null);
					}
				}else {
					if(this.filsDroit != null) {
						this.filsDroit.insererElement(element);
					}else {
						this.filsDroit = new ArbreBinaire<>(element, null, null);
					}
				}
			}
		}
	}
	
	private String versChaine() {
		if(estVide()) {
			return "";
		}
		String debut = "";
		if(this.filsGauche != null) {
			debut+=this.filsGauche.versChaine();
		}
		debut+=this.racine.toString()+", ";
		if(this.filsDroit != null) {
			debut+=this.filsDroit.versChaine();
		}
		return debut;
	}
	
	public void afficherInfixe() {
		if(!estVide()) {
			if(this.filsGauche != null) {
				this.filsGauche.afficherInfixe();
			}
			System.out.print(this.racine+", ");
			if(this.filsDroit != null) {
				this.filsDroit.afficherInfixe();
			}
		}
	}
	
	public void afficherPrefixe() {
		if(!estVide()) {
			System.out.print(this.racine+", ");
			if(this.filsGauche != null) {
				this.filsGauche.afficherPrefixe();
			}
			if(this.filsDroit != null) {
				this.filsDroit.afficherPrefixe();
			}
		}
	}
	
	public void afficherPostfixe() {
		if(!estVide()) {
			if(this.filsGauche != null) {
				this.filsGauche.afficherPostfixe();
			}
			if(this.filsDroit != null) {
				this.filsDroit.afficherPostfixe();
			}
			System.out.print(this.racine+", ");
		}
	}
	
	@Override
	public String toString() {
		String valeur = versChaine();
		return valeur.length() > 2 ? valeur.substring(0, valeur.length()-2) : valeur;
	}
	
	public boolean estVide() {
		return this.racine == null;
	}
	
	public void setRacine(T racine) {
		this.racine = racine;
	}

	public void setFilsDroit(ArbreBinaire<T> filsDroit) {
		this.filsDroit = filsDroit;
	}

	public void setFilsGauche(ArbreBinaire<T> filsGauche) {
		this.filsGauche = filsGauche;
	}

	public T getRacine() {
		return racine;
	}

	public ArbreBinaire<T> getFilsDroit() {
		return filsDroit;
	}

	public ArbreBinaire<T> getFilsGauche() {
		return filsGauche;
	}
	
	
}
