package fr.dutinfo.td.collectionneurs;

import java.util.Arrays;

public class PointEntree1 {

	static String tab1[] = {
			"ici", "tout", "va", "bien"
	};

	public static void main(String[] args) {
		for(int i = 0; i < tab1.length; i++) {
			System.out.println(tab1[i]);
		}
		System.out.println("==");
		for(String bout : tab1) {
			System.out.println(bout);
		}
		System.out.println("==");
		System.out.println(Arrays.toString(tab1));
	}
	
}
