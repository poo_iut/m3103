package fr.dutinfo.td.collectionneurs;

import java.util.Arrays;

public class PointEntree2 {

	static int[] tab2 = new int[10];
	
	public static void main(String[] args) {
		// En utilisant les boucles:
		for(int i = 0; i < 5; i++) {
			tab2[i] = 1;
		}
		
		for(int i = tab2.length-1; i >= tab2.length-5; i--) {
			tab2[i] = 0;
		}
		
		System.out.println(Arrays.toString(tab2));
		System.out.println("===");
		Arrays.fill(tab2, -1);

		Arrays.fill(tab2, 0, 5, 1);
		Arrays.fill(tab2, tab2.length-5, tab2.length, 0);

		System.out.println(Arrays.toString(tab2));
		Arrays.sort(tab2);
		System.out.println(Arrays.toString(tab2));
	}

}
