package fr.dutinfo.td.collectionneurs;

import java.util.Set;
import java.util.TreeSet;

public class PointEntree3 {

	public static void main(String[] args) {
		Set<Produit> produits = new TreeSet<>();
		produits.add(new Produit("Téléphone", 650));
		produits.add(new Produit("MicroOrdinateur", 2500));
		for(Produit produit : produits) {
			System.out.println(produit);
		}
	}

}
