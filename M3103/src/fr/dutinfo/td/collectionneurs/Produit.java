package fr.dutinfo.td.collectionneurs;

public class Produit implements Comparable<Produit>{

	private String libelle;
	private float prix;

	public Produit(String libelle, float prix) {
		this.libelle = libelle;
		this.prix = prix;
	}

	public String getLibelle() {
		return libelle;
	}

	public float getPrix() {
		return prix;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		result = prime * result + Float.floatToIntBits(prix);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produit other = (Produit) obj;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.libelle+" "+this.prix+"€";
	}
	
	@Override
	public int compareTo(Produit p) {
		return this.libelle.compareTo(p.libelle);
	}
	
	

}
