package fr.dutinfo.td.jdc;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class JeuDeCarte {

	private List<Carte> paquet;
	
	public JeuDeCarte() {
		this.paquet = new LinkedList<Carte>();
		for(Couleur couleur : Couleur.values()) {
			for(Valeur valeur : Valeur.values()) {
				paquet.add(new Carte(couleur, valeur));
			}
		}
	}
	
	public Carte premiereCarte() {
		if(paquet.isEmpty()) throw new ListeVide();
		return paquet.get(0);
	}
	
	public void distribuer() {
		paquet.remove(premiereCarte());
	}
	
	public String retourner() {
		Carte c = premiereCarte();
		Collections.rotate(this.paquet, -1);
		return c.toString();
	}
	
	public void ranger() {
		Collections.sort(paquet);
	}
	
	public void melanger() {
		Collections.shuffle(paquet);
	}
	
}
