package fr.dutinfo.tp2;

import java.util.ArrayList;
import java.util.List;

import fr.dutinfo.tp2.exception.ListeVide;

public class ListeCirculaire<T> {

	private List<T> liste;
	private int pointeur;
	
	/**
	 * Cr�ation d'une liste circulaire vide
	 */
	public ListeCirculaire() {
		this.liste = new ArrayList<T>();
		this.pointeur = 0;
	}
	
	/**
	 * Ajouter un item � une liste
	 * @param paramT
	 */
	public void ajouter(T paramT) {
		this.liste.add(paramT);
	}
	
	/**
	 * Retourne le premier element de la liste 
	 * @return T
	 */
	public T premier() {
		if(this.liste.isEmpty()) {
			throw new ListeVide();
		}
		this.pointeur = 0;
		return this.liste.get(this.pointeur);
	}
	
	/**
	 * Renvoie l'index d'un T
	 * @return
	 */
	public int indexDe(T paramT) {
		return this.liste.indexOf(paramT);
	}
	
	/**
	 * Retourne l'�lement suivant de la liste
	 * @return T
	 */
	public T suivant() {
		if(this.liste.isEmpty()) {
			throw new ListeVide();
		}
		int n = this.pointeur;
		this.pointeur++;
		if(this.pointeur >= this.liste.size()) {
			this.pointeur = 0;
		}
		return this.liste.get(n);
	}

	/**
	 * Retourne vrai si la liste contient T
	 * @param paramT
	 * @return
	 */
	public boolean contains(T paramT) {
		return this.liste.contains(paramT);
	}
	
}
