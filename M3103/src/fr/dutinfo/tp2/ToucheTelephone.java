package fr.dutinfo.tp2;

public class ToucheTelephone {

	private ListeCirculaire<Character> liste;
	
	/**
	 * 
	 * @param String contenu
	 */
	public ToucheTelephone(String contenu) {
		this.liste = new ListeCirculaire<Character>();
		for(char c : contenu.toCharArray()) {
			this.liste.ajouter(c);
		}
	}
	
	/**
	 * Retourne le nb-i�me caract�re
	 * @param nb
	 * @return
	 */
	public Character getCaractere(int nb) {
		Character c = this.liste.premier();
		for(int i = 0; i < nb; i++) {
			c = this.liste.suivant();
		}
		return c;
	}
	
	/**
	 * 
	 * @param character
	 * @return
	 */
	public int getNombreDeFois(Character character) {
		return this.liste.indexDe(character)+1;
	}
	
	/**
	 * Retoure vrai si la liste contiens le caract�re X
	 * @param character
	 * @return
	 */
	public boolean contains(Character character) {
		return liste.contains(character);
	}
	
	
}
