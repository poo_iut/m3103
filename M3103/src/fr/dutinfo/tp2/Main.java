package fr.dutinfo.tp2;

public class Main {

	public static void main(String[] args) {
		Telephone telephone = new Telephone();
		System.out.println(telephone.decoder("6622999"));
		System.out.println(telephone.decoder("44445555003333224444880022233322888"));
		System.out.println(telephone.decoderPoint("6622999.9999"));
		System.out.println(telephone.encode("#LaBiseJL"));
		System.out.println(telephone.encodeTest("#LaBiseJL"));
		System.out.println(telephone.decoder(telephone.encodeTest("#LaBiseJL")));
		System.out.println(telephone.decoder("#555522.222444477777333##0055.5555"));
	}

}
