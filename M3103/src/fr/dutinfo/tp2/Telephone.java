package fr.dutinfo.tp2;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Telephone {

	private String[] caracteres = {
			"0  ",
			"1",
			"2abc",
			"3def",
			"4ghi",
			"5jkl",
			"6mno",
			"7pqrs",
			"8tuv",
			"9wxyz"
	};
	
	private String[] nouveauCaracteres = {
			"0  ",
			"1",
			"2abc",
			"3def",
			"4ghi",
			"5jkl",
			"6mno",
			"7pqrs",
			"8tuv",
			"9wxyz",
			"#,;:",
			"*?."
	};
	
	private ToucheTelephone[] touche = new ToucheTelephone[this.nouveauCaracteres.length];
	
	/**
	 * Construit un t�l�phone
	 */
	public Telephone() {
		for(int i = 0; i < this.nouveauCaracteres.length; i++) {
			this.touche[i] = new ToucheTelephone(this.nouveauCaracteres[i]);
		}
	}
	
	
	/**
	 * Permet de d�coder un message SMS
	 * @param sms
	 */
	public String decoder(String sms) {
		sms = sms.toLowerCase();
		String fin = "";
		for(int i = 0; i < sms.length(); i++) {
			char c = sms.charAt(i);
			if(c == '.')continue;
			int nombre = 0;
			int y = i;
			for(; y < sms.length(); y++) {
				if(sms.charAt(y) != c) {
					break;
				}
				nombre++;
			}
			i=y-1;
			fin+=this.touche[chercherNumero(c)].getCaractere(nombre);
		}
		return fin;
	}
	
	/**
	 * Decode avec les selections
	 * @param sms
	 * @return
	 */
	public String decoderPoint(String sms) {
		sms = sms.toLowerCase();
		String fin = "";
		int nbr = 0;
		char c = ' ';
		int numero = -1;
		for(int i = 0; i < sms.length(); i++) {
			char actuel = sms.charAt(i);
			if(actuel == c) {
				nbr++;
			}else if(actuel == '.') {
				fin+=this.touche[numero].getCaractere(nbr);
				c = '.';
				numero = -1;
				nbr = 0;
			}else {
				if(numero != -1) {
					fin+=this.touche[numero].getCaractere(nbr);
				}
				c = actuel;
				numero = chercherNumero(c);
				nbr = 1;
			}
		}
		if(numero != -1) {
			fin+=this.touche[numero].getCaractere(nbr);
		}
		return fin;
	}
	
	
	public String encodeTest(String texte) {
		return texte.chars()
				.mapToObj(x -> (char) x)
				.map(c -> {
					ToucheTelephone touche = chercherTouche(c);
					if(touche == null)return ".";
					StringBuilder builder = new StringBuilder();
					IntStream.range(0, touche.getNombreDeFois(c))
					.mapToObj(i -> touche.getCaractere(0))
					.forEach(builder::append);
					return builder.append(".").toString();
				})
				.reduce("", String::concat);
	}
	
	
	/**
	 * Encode un texte en sms
	 * @param texte
	 * @return
	 */
	public String encode(String texte) {
		texte = texte.toLowerCase();
		String sms = "";
		for(int i = 0; i < texte.length(); i++) {
			char c = texte.charAt(i);
			ToucheTelephone touche = chercherTouche(c);
			if(touche == null) {
				continue;
			}
			int nbr = touche.getNombreDeFois(c);
			for(; nbr > 0; nbr--)sms+=touche.getCaractere(0);
			if(i < texte.length() -1) {
				char c2 = texte.charAt(i+1);
				ToucheTelephone touche2 = chercherTouche(c2);
				if(touche2 != null) {
					if(touche2.equals(touche)) {
						sms+=".";
					}
				}
				
			}
		}
		return sms;
	}
	
	/**
	 * Recherche l'objet ToucheTelephone en fonction du caract�re d'entre c
	 * @param c
	 * @return
	 */
	private ToucheTelephone chercherTouche(char c) {
		for(ToucheTelephone toucheTelephone : this.touche) {
			if(toucheTelephone.contains(c))return toucheTelephone;
		}
		return null;
	}
	
	/**
	 * Recherche le numero de c dans le tableau caract�re
	 * @param c
	 * @return
	 */
	private int chercherNumero(char c) {
		int numero = -1;
		switch (c) {
		case '#':
			numero = 10;
			break;
		case '*':
			numero = 11;
		default:
			numero = Integer.parseInt(String.valueOf(c));
			break;
		}
		return numero;
	}
}
